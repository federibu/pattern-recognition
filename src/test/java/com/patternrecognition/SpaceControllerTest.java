package com.patternrecognition;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.util.List;
import java.util.Random;
import java.util.Set;

import static com.patternrecognition.Space.center;
import static org.assertj.core.api.Assertions.assertThat;

public class SpaceControllerTest {
  static SpaceDAO spaceDAO = new SpaceDAO();
  SpaceController spaceController = new SpaceController(spaceDAO);

  @BeforeEach
  public void init() {
    spaceDAO.clear();
  }

  @Test
  public void testAddPoint() {
    Point point = new Point(1, 1);
    ResponseEntity<Point> responseEntity = spaceController.addPoint(point);

    assertThat(responseEntity.getStatusCode()).isEqualTo(HttpStatus.OK);
    assertThat(responseEntity.getBody()).isEqualTo(new Point(1, 1));
  }

  @Test
  void getSpaceTest() {
    //Given
    spaceDAO.add(center);

    //When
    ResponseEntity<Set<Point>> result = spaceController.getSpace();

    //Then
    assertThat(result.getBody()).isEqualTo(Set.of(center));
  }

  @Test
  void clearSpaceTest() {
    //Given
    spaceDAO.add(center);

    //When
    spaceController.deleteSpace();
    ResponseEntity<Set<Point>> result = spaceController.getSpace();

    //Then
    assertThat(result.getBody()).isEqualTo(Set.of());
  }

  @Test
  public void testGetOneLinesWithTwoPoints() {
    //Given
    Point point1 = new Point(1, 1);
    Point point2 = new Point(2, 2);
    spaceDAO.add(point1);
    spaceDAO.add(point2);

    //When
    ResponseEntity<List<Segment>> responseEntity = spaceController.getLines(2);

    //Then
    Segment expectedSegment = new Segment(Set.of(point1, point2), 1.0, 0.0);
    assertThat(responseEntity.getStatusCode()).isEqualTo(HttpStatus.OK);
    assertThat(responseEntity.getBody()).isEqualTo(List.of(expectedSegment));
  }

  @Test
  public void testGetTwoLinesWithThreePoints() {
    //Given
    Point point1 = new Point(1, 1);
    Point point2 = new Point(2, 2);
    Point point3 = new Point(-1, 1);
    Point point4 = new Point(-2, 2);
    spaceDAO.add(center);
    spaceDAO.add(point1);
    spaceDAO.add(point2);
    spaceDAO.add(point3);
    spaceDAO.add(point4);

    //When
    ResponseEntity<List<Segment>> responseEntity = spaceController.getLines(3);

    //Then
    Segment expectedSegment1 = new Segment(Set.of(center, point1, point2), 1.0, 0.0);
    Segment expectedSegment2 = new Segment(Set.of(center, point3, point4), -1.0, 0.0);
    assertThat(responseEntity.getStatusCode()).isEqualTo(HttpStatus.OK);
    assertThat(responseEntity.getBody()).isEqualTo(List.of(expectedSegment1, expectedSegment2));
  }

  @Test
  public void testGetLineBigNumbers() {
    //Given
    Random random = new Random();
    for (int i = 0; i < 1000; i++) {
      Point point = new Point(random.nextInt(), random.nextInt());
      spaceDAO.add(point);
    }

    //When
    ResponseEntity<List<Segment>> responseEntity = spaceController.getLines(2);

    //Then

    assertThat(responseEntity.getStatusCode()).isEqualTo(HttpStatus.OK);
    assertThat(responseEntity.getBody()).isNotEmpty();
  }
}
