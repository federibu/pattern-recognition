package com.patternrecognition;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Set;

@RestController
public class SpaceController {
    @Autowired
    private final SpaceDAO spaceDAO;

    @Autowired
    public SpaceController(SpaceDAO spaceDAO) {
        this.spaceDAO = spaceDAO;
    }
    @GetMapping(value = "/space")
    @ResponseBody
    public ResponseEntity<Set<Point>> getSpace() {
        return ResponseEntity.ok(spaceDAO.getSpace());
    }

    @GetMapping(value = "/lines/{n}")
    @ResponseBody
    public ResponseEntity<List<Segment>> getLines(@PathVariable int n) {
        return ResponseEntity.ok(spaceDAO.getLines(n));
    }

    @PostMapping(value = "/point")
    public ResponseEntity<Point> addPoint(@RequestBody Point point) {
        var newPoint = spaceDAO.add(point);
        return ResponseEntity.ok(newPoint);
    }

    @DeleteMapping(value = "/space")
    public void deleteSpace() {
        spaceDAO.clear();
    }
}
