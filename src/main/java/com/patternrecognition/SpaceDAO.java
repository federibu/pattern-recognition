package com.patternrecognition;

import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Set;

@Service
public class SpaceDAO {

    private final Space space = new Space();

    public Set<Point> getSpace() {
        return space.points;
    }

    public Point add(Point point) {
        space.update(point);
        space.points.add(point);
        return point;
    }

    public void clear() {
        space.points.clear();
        space.segments.clear();
    }

    public List<Segment> getLines(int n) {
        return space.segments
          .stream()
          .filter( segment -> segment.points.size() >= n)
          .toList();
    }
}
