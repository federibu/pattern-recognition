package com.patternrecognition;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.util.Set;

@Data
@AllArgsConstructor
public class Segment {
  public Set<Point> points;
  public double coefficient;
  public double shift;

  public boolean traverses(Point point) {
    if (this.points.size() < 2) return false;
    var result = this.coefficient * point.x + this.shift - point.y;
    return result == 0;
  }
}
