package com.patternrecognition;


import lombok.Data;

import java.util.*;

@Data
public class Space {
  public Set<Point> points;
  public List<Segment> segments;
  static Point center = new Point(0,0);

  public Space() {
    points = new HashSet<>();
    segments = new ArrayList<>();
  }

  public void update(Point newPoint) {
    List<Point> remainingPoints = new ArrayList<>(this.points);
    for (Segment segment : segments) {
        if (segment.traverses(newPoint)) {
          remainingPoints.removeAll(segment.getPoints());
          segment.points.add(newPoint);
        }
    }
    for (Point point: remainingPoints) {
      double coefficient = (double) (newPoint.y - point.y) / (newPoint.x - point.x);
      double shift = -(coefficient * point.x) + point.y;
      Set<Point> set = new HashSet<>();
      set.add(newPoint);
      set.add(point);
      Segment newSegment = new Segment(set, coefficient, shift);
      segments.add(newSegment);
    }
  }
}